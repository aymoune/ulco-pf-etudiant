
                import System.IO
                import Data.List
                import Data.Char
                
                display :: String -> String
                display word = intersperse ' ' [if c `elem` ['a'..'z'] then '?' else c | c <- word]
                --foction pour cacher les lettre--

                makeGuess :: String -> Char -> Int -> IO ()
                makeGuess word letter guesses
                    | letter `elem` word = play [if letter == c then toUpper letter else c | c <- word] guesses
                    | otherwise = play word (guesses - 1)
                --foction pour calculer le nombre d'essais restantes--               
                    
                play :: String -> Int -> IO ()						
                play word guesses
                    | word == map toUpper word = do
                        putStrLn $ display word
                        putStrLn "You Win!"
                    | guesses == 0 = do
                        putStrLn $ display word
                        putStrLn "You Lose..."
                    | otherwise = do
                        putStrLn $ "You have " ++ show guesses ++ " guesses left."
                        putStrLn $ display word
                        putStr "Guess a letter: "
                        hFlush stdout
                        userGuess <- getLine
                        case userGuess of
                            c:_ -> makeGuess word (toLower $ head userGuess) guesses
                            _ -> putStrLn "Please enter a letter a-z" >> play word guesses

                            {-erreur :: Int -> Nothing
                            erreur e =
                             if e==1
                              then "test 1"
                              else if e == 2
                               then "test 2"
                               else if e == 3
                                then"test 3"
                                else if e == 4
                                 then putStrLn"\   _______     \n\
                                                \|/      |    \n\
                                                \|      (_)   \n\
                                                \|      \|/   \n\
                                                \|       |    \n\
                                                \|      / \   \n\
                                         \   ____|___________ "      -}
                                              
                                 
                                 


                    
                main :: IO ()
                main = do
                    putStrLn "Welcome to Haskell Hangman!"
                    putStr "Enter a word (or sentence) to guess: "
                    hFlush stdout
                    userInput <- getLine -- Get a string from user and store it in userInput
                    case userInput of
                        c:_ -> play (map toLower userInput) 6 -- This last int is the amount of wrong guesses the player will have
                        _ -> putStrLn "Please input at least one character!" >> main
                    putStrLn "Thanks for playing!"


